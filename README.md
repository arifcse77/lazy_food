# Installing RabbitMQ on Ubuntu

### To install it on a newer Ubuntu version is very straightforward:
```angular2html
apt-get install -y erlang
apt-get install rabbitmq-server
```

### Then enable and start the RabbitMQ service:
```angular2html
systemctl enable rabbitmq-server
systemctl start rabbitmq-server
```

### Check the status to make sure everything is running smooth:

```angular2html
systemctl status rabbitmq-server
```

# How to run celery
```angular2html
celery -A lazy_food worker -l info
```
