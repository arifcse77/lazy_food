from django.urls import path

from . import views

urlpatterns = [
    path('', views.GenerateRandomUserView.as_view()),
]