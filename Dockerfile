FROM python:3.8
RUN apt-get update

WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt

EXPOSE 8000
CMD ["python", "manage.py", "migrate"]
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]